import { LitElement, html, css } from 'lit-element';
// import { io } from "socket.io-client";

class App extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      socket: { type: Object },
      messages: { type: Array },
      selectedValue: { type: String }
    };
  }

  constructor() {
    super();
    // this.socket = io('http://localhost:3000');
    this.title = 'Welcome to Combot';
    this.messages = [];
    this.selectedValue = '';

    // this.socket.on('chat message', (msg)  => {
    //   const item = document.createElement('li');
    //   item.textContent = msg;
    //   this.messages.push(msg);
    //   window.scrollTo(0, document.body.scrollHeight);
    // });

    // const Http = new XMLHttpRequest();
    // const url='http://localhost:3000/test';
    // Http.open("GET", url);
    // Http.send();
    // console.log('a');
    //
    // Http.onreadystatechange = (e) => {
    //   console.log(Http.responseText)
    // }
  }

  static get styles() {
    return [
      css`
        :host {
          text-align: center;
          min-height: 100vh;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          font-size: calc(10px + 2vmin);
          color: #1a2b42;
        }

        header {
          margin: auto;
        }

        svg {
          animation: app-logo-spin infinite 20s linear;
        }

        a {
          color: #217ff9;
        }

        .app-footer {
          color: #a8a8a8;
          font-size: calc(10px + 0.5vmin);
        }

        @keyframes app-logo-spin {
          from {
            transform: rotate(0deg);
          }
          to {
            transform: rotate(360deg);
          }
        }
      `
    ];
  }

  handleFormSubmit(e) {
    e.preventDefault();
    console.log(this.selectedValue);

    this.socket.emit('chat message', this.selectedValue);
    // input.value = '';
  }

  getMessagesTemplate() {
    const templates = [];

    // messages = this.messages;
    const messages = ['1', '2'];

    for (let i = 0; i < messages.length; i++) {
      templates.push(html`<li>Message: ${messages[i]}</li>`);
    }

    return templates;
  }

  render() {
    return html`
      <h1>${this.title}</h1>
      <ul id="messages">
        ${this.getMessagesTemplate()}
      </ul>
      <form id="form" .onsubmit=${this.handleFormSubmit.bind(this)}>
        <input
          id="input"
          autocomplete="off"
          value=${this.selectedValue}
        /><button>Send</button>
      </form>

      <p class="app-footer">SV</p>
    `;
  }
}

customElements.define('app-index', App);
